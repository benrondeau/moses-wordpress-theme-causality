<?php wp_head(); ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> > 
<head>
    <title><?php wp_title(' | ', true, 'right'); ?> <?php bloginfo('name'); ?> </title>

    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width" />
    <meta name="description" content="<?php bloginfo('description'); ?>">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.io">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/img/apple-touch-icon-precomposed.png">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" type="text/css" media="screen" />
	
    <!-- Scripts -->
    <!-- Javascript polyfill for IE browsers that don't support HTML5 elements. Must be in head of document. -->
    <!--[if lt IE 9]>
        <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/html5shiv/html5shiv.js"></script>
    <![endif]-->

</head>
<body <?php body_class(); ?> >
    <!--[if lt IE 8]>
        <h1>You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</h1>
    <![endif]-->
    <div class="container">
        <header class="clearfix">
            <section id="header-logo">
                <a href="<?php echo site_url(); ?>" class="moses-logo text-push-off-screen">Moses</a>
            </section>
            <nav>
                <section id="social-and-donate">
                    <a href="https://www.facebook.com/pages/MOSES-Metropolitan-Organizing-Strategy-Enabling-Strength/105837536190500" title="Facebook" class="facebook-header text-push-off-screen">Facebook</a>
                    <a href="https://twitter.com/moses_mi" class="twitter-header text-push-off-screen" title="Twitter">Twitter</a>
                    <a href="http://www.youtube.com/channel/UCPWCcWYi-SpgpHxFSY7oxVw" class="youtube-header text-push-off-screen" title="YouTube">Youtube</a>
                    <a href="" id="donate-button">DONATE NOW</a>
                </section>
                <section id="navigation">
                    <ul>
                        <!-- Dynamic Menu Set in WP-Admin -->
                        <?php wp_nav_menu( array ('menu' => 'Header_Menu') ); ?>
                    </ul>
                </section>
            </nav>
        </header>