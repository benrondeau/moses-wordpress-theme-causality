<?php 
/*
Template Name: Home Page
*/ 
?>

<?php get_header(); ?>

<!-- Hero Section -->
<section id="hero">
    <ul id="hero-buttons">
        <li><a href="<?php echo site_url(); ?>/issues/#economic-dignity" id="hero-button-yellow">ECONOMIC DIGNITY</a></li>
        <li><a href="<?php echo site_url(); ?>/issues/#healthy-communities" id="hero-button-darkyellow">Healthy & SUSTAINABLE Communities</a></li>
        <li><a href="<?php echo site_url(); ?>/issues/#education-for-all" id="hero-button-orange">EDUCATION FOR ALL</a></li>
        <li><a href="<?php echo site_url(); ?>/issues/#transparency-and-accountability" id="hero-button-purple">TRANSPARENCY and ACCOUNTABILITY</a></li>
        <li><a href="<?php echo site_url(); ?>/issues/#transportation-equity" id="hero-button-red">TRANSPORTATION EQUITY</a></li>
    </ul>
    <div id="home-slider">
        <!-- Insert Slider Script Here -->
        <img src="<?php echo get_template_directory_uri(); ?>/img/hero-image.jpg" alt="Moses Community">
    </div>
</section>

<!-- Get Content From WP-Admin Page Editor -->
<?php ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php the_content(); ?>
<?php endwhile; ?>
<?php endif; ?>


<?php get_footer(); ?>