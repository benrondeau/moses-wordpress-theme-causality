<?php 
/**
 * functions.php
 *
 * Core functions that setup theme.
 *
 * @package WordPress
 * @subpackage MOSES Theme by Causality
 * @since 1.0
 *
 */


/***** Constants *****/

// $content_width



/***** Theme Setup *****/

//Conditional statement in the case this is being used a child theme. 2 function files can create issues.
if ( ! function_exists( 'moses_theme_setup' ) ) {

	//Setup for Theme
	function moses_theme_setup() {


		//Sets maximum width for any theme content
		if ( ! isset( $content_width ) ) {
			$content_width = 600;//px value
		}

		//Adds support for navigation menu in WP-Admin
		register_nav_menus(
			array(
					// 'Slug placed in theme files to call this menu' => 'Description in WP Admin'
					'Header_Menu' => 'Header Menu',
					// 'Footer_Menu' => 'Footer Menu'
				)
		);

		//Adds support for automatic feed lins for RSS
		add_theme_support( 'automatic-feed-links' );


		//Adds support for HTML5 elements in array items
		add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );


		//Page Sidebar
		// register_sidebar(
		// 	array(
		// 		'name' => 'Page Sidebar',
		// 		'id' => 'sidebar',
		// 		'description' => 'Sidebar that appears on the pages'
		// 	)
		// );

	}//END moses_theme_setup()

};// END if


//Runs moses_theme_setup function after theme is setup by WP
add_action( 'after_setup_theme', 'moses_theme_setup');


 ?>