    </div> <!-- END .container -->
<!-- Full Width Footer -->
    <footer>
        <!-- Center content in footer -->
        <div class="container">
            <div id="footer-contact-us">
                <div class="moses-white-logo"></div>
                <div id="footer-contact-info">
                    <h2>Contact Us</h2>
                    <p class="white-bottom-border"></p>
                    <p><span style="font-size:13px;font-weight:bold;">Metropolitan Organizing Strategy Enabling Strength (MOSES)</span><br><em>an affiliate of the Gamaliel Foundation</em></p>
                    <p>220 Bagley Street, Suite, 212 Detroit, MI 48220 <br/>Phone: (313) 962-5290  Fax: (313) 262-6649  Email: mosesmi@mosesmi.org 
                    </p>
                    <p>MOSES is a non-partisan 501c3 organization. <br/>Contributions made to MOSES are tax-deductible to the full extent permitted by law.</p>
                </div>
            </div>
            <div id="footer-follow-us">
                <h2>Follow Us</h2>
                <p class="white-bottom-border"></p>
                <ul>
                    <li><a href="https://www.facebook.com/pages/MOSES-Metropolitan-Organizing-Strategy-Enabling-Strength/105837536190500" title="Facebook" class="text-push-off-screen facebook-footer">Facebook</a></li>
                    <li><a href="https://twitter.com/moses_mi" class="text-push-off-screen twitter-footer" title="Twitter">Twitter</a></li>
                    <li><a href="http://www.youtube.com/channel/UCPWCcWYi-SpgpHxFSY7oxVw" class="text-push-off-screen youtube-footer" title="YouTube">Youtube</a></li>
                </ul>
                <p><a href="<?php echo site_url(); ?>/sitemap/">Site Map</a> | <a href="">Privacy Policy</a></p>
                
            </div>
            <div id="footer-action-alert">
                <h2>Action Alert!</h2>
                <p>Sign up for upcoming opportunities to take action to better your community.</p>
                <img src="<?php echo get_template_directory_uri(); ?>/img/email-subscribe-form.png" alt="">
            </div>
        </div>
    </footer>


    <!-- Javascript Files -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>

</body>
</html>

<?php wp_footer(); ?>