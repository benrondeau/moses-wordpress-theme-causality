<?php 
/*
Posts dislay using this template.
*/ 
?>

<?php get_header(); ?>


<section id="interior-content" class="clearfix">
    <div id="interior-header-image">
        <h1><?php the_title(); ?></h1>
    </div>
    <article id="interior-page-text">
    	<!-- Get Content from WP-Admin Page Editor -->
	    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	        	<?php the_content(); ?>
	        <?php endwhile; ?>
		<?php endif; ?>
    </article>
    <aside id="interior-page-quote">
        <p>MOSES ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad miniveniamd exercitation ullamco laboris nisi ut aliquip ex ea commodo aute consequat.</p>
    </aside>
</section>
		

<?php get_footer(); ?>




